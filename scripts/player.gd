class_name Player

extends CharacterBody2D

@onready var animated_sprite: AnimatedSprite2D = $AnimatedSprite2D

@onready var world_gravity: float = ProjectSettings.get_setting("physics/2d/default_gravity")

@export_category("gravity")
@export var gravity_modifier: float = 1.0
@export var gravity: float = 400.0
@export var max_falling_velocity: float = 500

@export_category("movement")
@export var speed: float = 250.0
@export var jump_force: float = 200

var player_active: bool = true

# Add kyotey time
# Press down mid air to drop quicker

func _physics_process(delta):
	if !is_on_floor():
		if  velocity.y < max_falling_velocity:
			velocity.y += (gravity * gravity_modifier) * delta
	
	var direction: float = 0.0
	
	if player_active:
		if Input.is_action_just_pressed("jump") && is_on_floor():
			jump(jump_force)
		
		direction = Input.get_axis("move_left", "move_right")
	
	if direction != 0.0:
		animated_sprite.flip_h = (direction == -1.0)
	
	velocity.x = direction * speed
	
	move_and_slide()
	
	update_animations(direction)

func jump(force):
	AudioPlayer.play_sfx(Constants.Sfx.JUMP)
	velocity.y = -force

func update_animations(direction: float):
	if is_on_floor():
		if direction == 0:
			animated_sprite.play("idle")
		else:
			animated_sprite.play("run")
	else:
		if velocity.y < 0:
			animated_sprite.play("jump")
		else:
			animated_sprite.play("fall")

