class_name EndPoint

extends Area2D

@onready var sprite: AnimatedSprite2D = $AnimatedSprite2D

func animate():
	sprite.play()
