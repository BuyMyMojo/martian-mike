@tool

extends AnimationPlayer

@export var start_time: float = 0.0

func _ready():
	self.seek(start_time, true)
