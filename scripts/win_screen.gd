class_name WinScreen

extends Control

@onready var thank_you_player: AnimationPlayer = $ThankYouLayer/AnimationPlayer
@onready var middle_finger: TextureRect = $ThankYouLayer/MiddleFinger

func _on_button_pressed():
	get_tree().change_scene_to_file("res://scenes/main_menu.tscn")


func _on_thank_you_pressed():
	middle_finger.visible = true
	thank_you_player.play("MiddleFinger")

	await get_tree().create_timer(13.5).timeout
	middle_finger.visible = false
