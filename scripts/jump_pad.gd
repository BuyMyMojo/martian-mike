extends Area2D

@onready var animated_sprite: AnimatedSprite2D = $AnimatedSprite2D

@export var jump_force: float = 300

func _on_body_entered(body):
	if body is Player:
		animated_sprite.play("jump")
		
		if body.velocity.y > 0:
			body.jump(jump_force)
		else:
			body.jump(-body.velocity.y + jump_force)
