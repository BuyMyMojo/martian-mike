extends Node

var music_player: AudioStreamPlayer

func _ready():
	music_player = AudioStreamPlayer.new()
	music_player.stream = preload("res://assets/audio/music.ogg")
	music_player.autoplay = true
	music_player.bus = "Music"
	
	add_child(music_player)
