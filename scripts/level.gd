extends Node2D

@export_category("Player Related")
@export var level_start: StartPoint
@export var level_end: EndPoint

@export_category("Level")
@export var next_level: PackedScene = null
@export var level_time: float = 5
@export var is_final_level: bool = false


@onready var death_zone: Area2D = $DeathZone
@onready var hud: Hud = $UILayer/HUD
@onready var game_ui: GameUI = $UILayer

var player: Player = null
var level_timer: Timer = null
var time_left: float

var win: bool = false

func _ready():
	game_ui.set_win_screen_visibility(false)
	
	player = get_tree().get_first_node_in_group("player")
	
	var traps: Array[Node] = get_tree().get_nodes_in_group("traps")
	
	for trap in traps:
		if trap is Trap:
			trap.touched_player.connect(_on_trap_touched_player)
	
	if player != null:
		player.global_position = level_start.get_spawn_pos()
	
	level_end.body_entered.connect(_on_exit_body_entered)
	
	death_zone.body_entered.connect(_on_death_zone_body_entered)
	
	time_left = level_time
	hud.set_time_label(time_left)
	
	
	level_timer = Timer.new()
	level_timer.name = "LevelTimer"
	level_timer.wait_time = 1
	level_timer.timeout.connect(_on_level_timer_timeout)
	add_child(level_timer)
	level_timer.start()

func _on_level_timer_timeout():
	if !win:
		time_left -= 1
		
		hud.set_time_label(time_left)
		
	
		if time_left < 0:
			AudioPlayer.play_sfx(Constants.Sfx.HURT)
			reset_player()
			time_left = level_time
			hud.set_time_label(time_left)

func _process(_delta):
	if Input.is_action_just_pressed("quit"):
		get_tree().quit()
	elif Input.is_action_just_pressed("reset"):
		get_tree().reload_current_scene()


func _on_death_zone_body_entered(_body):
	AudioPlayer.play_sfx(Constants.Sfx.HURT)
	reset_player()


func _on_trap_touched_player():
	AudioPlayer.play_sfx(Constants.Sfx.HURT)
	reset_player()

func _on_exit_body_entered(body):
	if body is Player:
		if is_final_level || (next_level != null):
			level_end.animate()
			player.player_active = false
			win = true
			await get_tree().create_timer(1.5).timeout
			if is_final_level:
				game_ui.set_win_screen_visibility(true)
			else:
				get_tree().change_scene_to_packed(next_level)
		
		else:
			print_rich("[shake rate=20.0 level=5 connected=1][color=purple][font_size=32][b]Oopsie??[/b][/font_size][/color][/shake]")

func reset_player():
	player.velocity = Vector2.ZERO
	player.global_position = level_start.get_spawn_pos()
