class_name Trap

extends Node2D

signal touched_player

func _on_area_2d_body_entered(body):
	if body is Player:
		touched_player.emit()

@export var start_time: float = 0.0

func _ready():
	$AnimationPlayer.seek(start_time)
