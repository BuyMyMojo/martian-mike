class_name AudioManager

extends Node

var hurt_sound: AudioStreamWAV = preload("res://assets/audio/hurt.wav")
var jump_sound: AudioStreamWAV = preload("res://assets/audio/jump.wav")

func play_sfx(sfx_type: Constants.Sfx):
	var stream: AudioStream = null
	
	match sfx_type:
		Constants.Sfx.JUMP:
			stream = jump_sound
		Constants.Sfx.HURT:
			stream = hurt_sound
		null:
			printerr("NO SFX TYPE GIVEN!")
			return
	
	var audio_player: AudioStreamPlayer = AudioStreamPlayer.new()
	audio_player.stream = stream
	audio_player.name = "SFX"
	audio_player.bus = "Game"
	
	add_child(audio_player)
	
	audio_player.play()
	
	await audio_player.finished
	
	audio_player.queue_free()
