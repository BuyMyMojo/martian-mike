extends CanvasLayer

@onready var main_menu: Control = $MainMenu
@onready var options_menu: Control = $OptionsMenu
@onready var controls_display: Control = $ControlsDisplay

@onready var master_audio_bus := AudioServer.get_bus_index("Master")
@onready var music_audio_bus := AudioServer.get_bus_index("Music")
@onready var game_audio_bus := AudioServer.get_bus_index("Game")
@onready var master_current_volume_label: Label = $OptionsMenu/VBoxContainer/HSplitContainer/VSplitContainer/CurrentVolume
@onready var music_current_volume_label: Label = $OptionsMenu/VBoxContainer/MusicVolume/VSplitContainer/CurrentVolume
@onready var game_current_volume_label: Label = $OptionsMenu/VBoxContainer/GameVolume/VSplitContainer/CurrentVolume

var first_level: PackedScene = preload("res://scenes/level.tscn")

var current_audio_value: float = 100

func _ready():
	AudioServer.set_bus_volume_db(music_audio_bus, linear_to_db(0.5))
	AudioServer.set_bus_volume_db(game_audio_bus, linear_to_db(0.8))
	main_menu.visible = true
	options_menu.visible = false

func _on_start_pressed():
	get_tree().change_scene_to_packed(first_level)


func _on_quit_pressed():
	get_tree().quit()


func _on_options_pressed():
	main_menu.visible = false
	options_menu.visible = true
	controls_display.visible = false


func _toggle_full_screen():
	match DisplayServer.window_get_mode():
		DisplayServer.WINDOW_MODE_FULLSCREEN:
			DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED)
		_:
			DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
		


func _on_options_back_pressed():
	main_menu.visible = true
	options_menu.visible = false
	controls_display.visible = false


func _on_master_volume_slider_value_changed(value: float):
	AudioServer.set_bus_volume_db(master_audio_bus, linear_to_db(value))
	master_current_volume_label.text = "Current: %s" % str(remap(value, 0, 1, 0, 100))


func _on_music_volume_slider_value_changed(value: float):
	AudioServer.set_bus_volume_db(music_audio_bus, linear_to_db(value))
	music_current_volume_label.text = "Current: %s" % str(remap(value, 0, 1, 0, 100))


func _on_game_volume_slider_value_changed(value: float):
	AudioServer.set_bus_volume_db(game_audio_bus, linear_to_db(value))
	game_current_volume_label.text = "Current: %s" % str(remap(value, 0, 1, 0, 100))


func _on_controls_pressed():
	main_menu.visible = false
	options_menu.visible = false
	controls_display.visible = true
