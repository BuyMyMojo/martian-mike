class_name Hud

extends Control

@onready var time_label: Label = $TimeLabel

func  set_time_label(value: float):
	
	time_label.text = "Time: %d" % value
