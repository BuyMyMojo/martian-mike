@tool

extends ParallaxBackground

@export var bg_texture: CompressedTexture2D = preload("res://assets/textures/bg/Purple.png")
@export var scroll_speed: float = 15


@onready var sprite: Sprite2D = $ParallaxLayer/Sprite2D

func _ready():
	sprite.texture = bg_texture

func _process(delta):
	sprite.region_rect.position += delta * Vector2(scroll_speed, scroll_speed)
	
	if sprite.region_rect.position >= Vector2(bg_texture.get_size().x, bg_texture.get_size().y):
		sprite.region_rect.position = Vector2.ZERO

