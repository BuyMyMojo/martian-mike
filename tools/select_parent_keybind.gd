@tool
extends Node

var instant_press: bool = true

func _process(_delta):
	if is_keybind_pressed() && instant_press == true:
		var selected_nodes: Array[Node] = EditorInterface.get_selection().get_selected_nodes()
		
		if selected_nodes.size() == 1:
			var parent: Node = selected_nodes[0].get_parent()
			EditorInterface.get_selection().clear()
			await get_tree().create_timer(0.05).timeout
			EditorInterface.edit_node(parent)
		
		instant_press = false
	elif is_keybind_pressed() == false && instant_press == false:
		instant_press = true

func is_keybind_pressed() -> bool:
	return Input.is_key_pressed(KEY_CTRL) && Input.is_key_pressed(KEY_J)
